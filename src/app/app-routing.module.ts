import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AddQuestionComponent } from './add-question/add-question.component';
import { ProfileComponent } from './profile/profile.component';
import { ResponseListComponent } from './response-list/response-list.component';
import { ContactusComponent } from './contactus/contactus.component';
import { ResponseEditComponent } from './response-edit/response-edit.component';
import { AuthGuard } from './Services/auth-gaurd.service';
import { CaseEditResolveService } from './Services/case-edit-resolve.service';
import { ViewCaseComponent } from './view-case/view-case.component';
import { VerificationComponent } from './verification/verification.component';


const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'verification', component: VerificationComponent,pathMatch:'full'

  },
  {
    path: 'home', component: HomeComponent,pathMatch: 'full'
  },
  {
    path: 'responses', component: ResponseListComponent, pathMatch: 'full', canActivate: [AuthGuard],

  },
  {
    path: 'contactus', component: ContactusComponent, pathMatch: 'full'
  },
  {
    path: 'add-question', component: AddQuestionComponent, canActivate: [AuthGuard],pathMatch:'full'

  },
 
  {
    path: 'edit-response/:caseID', component: ResponseEditComponent,pathMatch: 'full', canActivate: [AuthGuard],
    resolve: {
      case: CaseEditResolveService
    }

  },

  {
    path: 'view-case/:caseID', component: ViewCaseComponent,pathMatch: 'full', canActivate: [AuthGuard],
    resolve: {
      case: CaseEditResolveService
    }

  },
  {
    path: 'profile', canActivate: [AuthGuard],
    component: ProfileComponent
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

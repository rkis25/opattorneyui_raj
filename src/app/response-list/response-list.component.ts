import { Component, OnInit } from '@angular/core';
import { CoreDataService } from '../Services/core-data.service';
import { SharedDataService } from '../Services/shared-data.service';
import { CaseResponseHistory } from '../Models/caseResponseHistory';
import { NgxPaginationModule } from 'ngx-pagination';
import { AuthService } from '../Services/auth.service';
@Component({
  selector: 'app-response-list',
  templateUrl: './response-list.component.html',
  styleUrls: ['./response-list.component.scss']
})
export class ResponseListComponent implements OnInit {
  public pendingQuesList: CaseResponseHistory[] = [];
  public completedQuesList: CaseResponseHistory[] = [];
  public myCases: any[] = [];
  public selectPendingStatus: boolean = true;
  public selectCompletedStatus: boolean = false;
  public p1: any;
  public p2: any;
  filteredCases: any[] = [];
  public user: any;
  constructor(private coreDataSvc: CoreDataService,
    public authService: AuthService,
    private sharedDataSvc: SharedDataService) { }

  ngOnInit() {
    this.GetMyResponses();
    this.authService.user$.subscribe(user => {
      this.user = user;
    });
  }
  onChange(event) {
    if (this.selectCompletedStatus && this.selectPendingStatus) {
      this.filteredCases = this.myCases;
      return;
    }
    if (this.selectCompletedStatus) {
      this.filteredCases = this.completedQuesList;

    } else if (this.selectPendingStatus) {
      this.filteredCases = this.pendingQuesList;
    }
    else {
      this.filteredCases = [];
    }
  }

  GetMyResponses() {
    this.coreDataSvc.getMyResponseHistory().subscribe((resp: any) => {
      console.log(resp);
      if (resp && resp.myCases) {

        this.pendingQuesList = resp.myCases.PENDING;
        this.completedQuesList = resp.myCases.COMPLETED;
        // this.completedQuesList = [];

        var cdata = this.pendingQuesList;
        if (cdata.length > 0) {
          var temp = Object.assign([], cdata);
          temp.map((x) => {
            if (x.responseLevel != 'Attorney') {
              x.response = "";
              x.responseLevel = "";
              x.paralegalResponseDate = null;
              x.attorneyResponseDate = null;
            }
            return x;
          });



          temp = temp.filter((thing, index, self) =>
            index === self.findIndex((t) => (
              t.caseID === thing.caseID && t.caseHeader === thing.caseHeader
            ))
          );
          //  console.log(temp);
          this.pendingQuesList = temp;

          //completed
          var qdata = this.completedQuesList;
          if (qdata.length > 0) {
            var temp1 = Object.assign([], qdata);
            temp1.map((x) => {
              if (x.responseLevel != 'Attorney') {
                x.response = "";
                x.responseLevel = "";
                x.paralegalResponseDate = null;
                x.attorneyResponseDate = null;
              }
              return x;
            });



            temp1 = temp1.filter((thing, index, self) =>
              index === self.findIndex((t) => (
                t.caseID === thing.caseID && t.caseHeader === thing.caseHeader
              ))
            );
            //  console.log(temp);
            this.completedQuesList = temp1;

            //end

            console.log(this.completedQuesList);
            if (this.completedQuesList) {
              this.myCases = Object.assign([], this.completedQuesList);
            }

            if (this.pendingQuesList && this.pendingQuesList.length > 0) {
              this.myCases.push(...this.pendingQuesList);
            }
            console.table(this.myCases);
            this.filteredCases = this.myCases;
          }

        }
      }

    }, err => {

      console.log(err);

      this.sharedDataSvc.showError("An error occurred fetching your cases..");

    })
  }
}

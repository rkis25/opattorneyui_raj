import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import { faPowerOff} from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/Services/auth.service';
import { SharedDataService } from 'src/app/Services/shared-data.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @ViewChild('navBurger',{static:false}) navBurger: ElementRef;
  @ViewChild('navMenu',{static:false}) navMenu: ElementRef;
  faPowerOff=faPowerOff;
  user: any;
  constructor(public authService:AuthService,private sharedDataSvc:SharedDataService) { }

  ngOnInit() {

    
    this.authService.user$.subscribe(user => {
      this.user = user;
    });
  }
  toggleNavbar() {
    this.navBurger.nativeElement.classList.toggle('is-active');
    this.navMenu.nativeElement.classList.toggle('is-active');
}

signOut() {
  this.authService.logout();
  this.sharedDataSvc.showRegisterMessage$.next(false);
  this.authService.user$.next(null);
 
}
}

import { Component, OnInit } from '@angular/core';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../Services/auth.service';
import { CoreDataService } from '../Services/core-data.service';
import { SharedDataService } from '../Services/shared-data.service';
import { CaseHistory } from '../Models/caseHistory';
import { NgxPaginationModule } from 'ngx-pagination';
@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.scss']
})
export class AddQuestionComponent implements OnInit {
  public faQuestion = faQuestionCircle;
  public p: any;
  public questionForm: FormGroup;
  public questionHistory: CaseHistory[] = [];
  public showSuccess: boolean = false;
  public showError: boolean = false;
  public questionObj: { subject: string, question: string, response: string, date: Date };
  public user: any;
  askForProfile: boolean = false;
  constructor(private fb: FormBuilder,
    public authService: AuthService,
    public coreDataService: CoreDataService,
    public sharedDataSvc: SharedDataService
  ) { }
  ngOnInit() {

    this.questionForm = this.fb.group({

      caseHeader: ['', Validators.required],
      description: ['', Validators.required]


    });
    this.GetMyCaseHistory();

    this.authService.user$.subscribe(user => {
      if (user) {
        this.user = user;
        this.askForProfile = this.user.isProfileComplete;
      }

    });


  }
  AddQuestion() {
    if (this.questionForm.valid) {
      this.coreDataService.addNewCase(this.questionForm.value)
        .subscribe((resp: any) => {
          console.log(resp);
          if (resp != undefined && resp != null) {
            this.sharedDataSvc.showInfo("Question Posted Successfully");
            //  this.GetMyCaseHistory();
            this.questionHistory = resp.myCases;
            this.showSuccess = true;
            this.showError = false;
            this.questionForm.reset();
          }

        }, err => {

          console.log(err);
          this.questionHistory = [];
          this.sharedDataSvc.showError("An error occurred while posting question, please contact support!");
          this.showError = true;
          this.showSuccess = false;
        });
    }

  }


  GetMyCaseHistory() {
    this.coreDataService.getMyCaseHistory().subscribe((resp: any) => {
      console.log(resp);
      var data = resp.myCases;
      if (data.length > 0) {
        var temp=Object.assign([],data);
       temp.map((x) => {
          if (x.responseLevel != 'Attorney') {
            x.response = "";
            x.responseLevel = "";
            x.paralegalResponseDate = null;
            x.attorneyResponseDate = null;
          }
          return x;
        });



        temp = temp.filter((thing, index, self) =>
          index === self.findIndex((t) => (
            t.caseID === thing.caseID && t.caseHeader === thing.caseHeader
          ))
        );
        console.log(temp);
        this.questionHistory = temp;
      }
      // this.questionHistory = resp.myCases;



    }, err => {

      console.log(err);
      this.questionHistory = [];

      this.sharedDataSvc.showError("An error occurred fetching your cases..");

    })
  }
  profComplete: boolean = false;
  onAuthTypeSwitch(authType: boolean) {
    this.askForProfile = authType;

  }

}

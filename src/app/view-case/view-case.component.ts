import { Component, OnInit } from '@angular/core';
import { AuthService } from '../Services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { CoreDataService } from '../Services/core-data.service';
import { SharedDataService } from '../Services/shared-data.service';
import { CaseHistory } from '../Models/caseHistory';

@Component({
  selector: 'app-view-case',
  templateUrl: './view-case.component.html',
  styleUrls: ['./view-case.component.scss']
})
export class ViewCaseComponent implements OnInit {
  public isDisabled: boolean = false;
  public caseObj: any = { caseID: 1001 };

  public questionObj:CaseHistory=new CaseHistory();
  public responseHistory:CaseHistory[]=[];
  user: any;
  constructor( private route: ActivatedRoute, 
    private coreDataSvc:CoreDataService,
    private sharedDataSvc:SharedDataService,
    public authService: AuthService) { }

  ngOnInit() {
    this.route.data
    .subscribe((data: { case: any }) => {
      if (data != undefined && data.case != undefined) {
        console.log(data);
        this.responseHistory = data.case;//.filter(x=>x.caseStatus==="COMPLETED");
        this.questionObj=data.case[0];
     
      }
    });
    this.authService.user$.subscribe(user => {
      this.user = user;
    });
  }

}

import { Injectable, ViewContainerRef } from '@angular/core';
import { CoreDataService } from './core-data.service';
import { ToastrService } from 'ngx-toastr';

import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {

  constructor(private coreDataService: CoreDataService, public toastr: ToastrService) {
  }


public showRegisterMessage:boolean=false;
showRegisterMessage$=new BehaviorSubject<boolean>(this.showRegisterMessage)
  public zipDetails: any = { State: '', City: '', ZipCode: '', Adress1: '' };
  zipDetails$ = new BehaviorSubject<any>(this.zipDetails);


public immigrationStatus=['OPT 12-month', 'OPT STEM', 
'CPT Out-of-Status', 
  'Other'];
  // public lawnSchedules: any = {};
  // lawnSchedules$ = new BehaviorSubject<any>(this.lawnSchedules);

  // public selectLawn: any;
  // selectLawn$ = new BehaviorSubject<any>(this.selectLawn);


  public sharedData: [] = [];
  sharedData$ = new BehaviorSubject<any>(this.sharedData);




  public showEmptyNavBar: boolean = true;

  errorMessage = '';
  vcr: ViewContainerRef;
  public UserInfo: any = {};




  public isLoggedIn: boolean = false;
  broadcastLoginStatus$ = new BehaviorSubject<any>(this.isLoggedIn);


  //// toaster messages
  showSuccess(message) {
    this.toastr.success(message, 'Success!');
  }

  showError(message) {
    this.toastr.error(message, 'Oops!');
  }

  showWarning(message) {
    this.toastr.warning(message, 'Alert!');
  }

  showInfo(message) {
    this.toastr.info(message);
  }

  // showCustom(template) {
  //   this.toastr.custom(template, null, { enableHTML: true });
  // }

}






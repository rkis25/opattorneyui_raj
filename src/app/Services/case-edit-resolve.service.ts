import { Injectable } from '@angular/core';
import { CoreDataService } from './core-data.service';
import { Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { take, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CaseEditResolveService {
  constructor(private coreDataService: CoreDataService, private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<never> {
    let id = route.paramMap.get('caseID');

    return this.coreDataService.getMyResponsesByCaseID(id).pipe(
      take(1),
      mergeMap((cases:any) => {
        if (cases ) {

          return of(cases.myCases);
        } else { // id not found
         // this.router.navigate(['']);
          return EMPTY;
        }
      })
    );
  }
}

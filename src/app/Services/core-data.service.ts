import { Injectable } from '@angular/core';
import { Observable, of, combineLatest  } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap, retry, shareReplay, zip, mergeMap, takeUntil } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CoreDataService {


  private baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }
//Product Related ops
addNewCase(question:any){
  const saveQues$ = this.http.post(this.baseUrl + '/candidate/addNewQuestion', question);
  const fetchQHistory$ = saveQues$.pipe(mergeMap(()=>{
    return  this.getMyCaseHistory();
   }));
  
   return fetchQHistory$
   .pipe( tap(this.extractData),
     catchError(this.handleError));
  //  .subscribe(([group, buddies, genres, region]) => {});
  // return this.http.post(this.baseUrl + '/candidate/addNewQuestion', question)
  
  // .pipe(
  //   tap(this.extractData),
  //   catchError(this.handleError),
  //   mergeMap(()=>{
  //     return  this.getMyCaseHistory();
  //    })
  //   );
}
addNewResponse(caseResponse:any){

const caseID=caseResponse.caseID;
  const saveQues$ = this.http.post(this.baseUrl + '/consultant/addNewResponse', caseResponse);
  const fetchQHistory$ = saveQues$.pipe(mergeMap(()=>{
    return  this.getMyResponsesByCaseID(caseID);
   }));
  
   return fetchQHistory$
   .pipe( tap(this.extractData),
     catchError(this.handleError));
  // return this.http.post(this.baseUrl + '/consultant/addNewResponse', caseResponse)
 
  // .pipe(
  //   // mergeMap(()=>{
  //   //   return  this.getMyResponseHistory();
  //   //  })
  //   tap(this.extractData),
  //   catchError(this.handleError)
    
    
  //   );
}




getMyCaseHistory(){
  return this.http.get(this.baseUrl+'/candidate/getMyCaseHistory')
      .pipe(
        tap(this.extractData),
        retry(3),
        catchError(this.handleError));
}

verifyUserToken(token) {
  return this.http.post(this.baseUrl + '/users/verifyUser', token)
    .pipe(
      tap(this.extractData),
      catchError(this.handleError));
}

getMyProfile(){
  return this.http.get(this.baseUrl+'/candidate/getMyProfile')
      .pipe(
        tap(this.extractData),
        retry(3),
        catchError(this.handleError));
}

getMyResponseHistory(){
  return this.http.get(this.baseUrl+'/consultant/getMyResponseHistory')
      .pipe(
        tap(this.extractData),
        retry(3),
        catchError(this.handleError));
}

getMyResponsesByCaseID(caseID){
  return this.http.get(this.baseUrl+'/consultant/getMyResponsesByCaseID',{params:{
    caseID:caseID
  }})
      .pipe(
        tap(this.extractData),
        retry(1),
        catchError(this.handleError));
}



getPreviousCases(userID){
  return this.http.get(this.baseUrl+'/products/getProductViewModel',{params: {
    userID: userID,
  }})
      .pipe(
        tap(this.extractData),
        retry(3),
        catchError(this.handleError));
}







getSharedData(){
  return this.http.get(this.baseUrl + '/common/getSharedData' )
      .pipe(
        tap(this.extractData),
        retry(3),
        catchError(this.handleError));
}

//sign in /sign up related ops
registerUser(user: any): Observable<any> {
  return this.http.post(this.baseUrl + '/users/signup', user)
    .pipe(
      tap(this.extractData),
      catchError(this.handleError));

}

loginUser(email: any, password: any): Observable<any> {
  return this.http.post(this.baseUrl + '/users/signin', { email, password})
    .pipe(
      tap(this.extractData),
      catchError(this.handleError));
}
//Save Profle
saveProfile(profile:any): Observable<any> {
  return this.http.post(this.baseUrl + '/users/completeProfile', profile)
    .pipe(
      tap(this.extractData),
      catchError(this.handleError));
}


    // common applicable  operations on http response object
    private extractData(res: Response) {
      const body = res || res.json();
      return body || {};
    }
    private handleError(errorObj: Response | any) {
      //  will use a remote logging infrastructure
      let errMsg: string;
      if (errorObj instanceof Response) {
        const body = errorObj || '';
        const err = body || JSON.stringify(body);
        errMsg = ` ${errorObj.status}-${errorObj.status} - ${errorObj.statusText || ''} ${err}`;
      }
      else if (errorObj.error) {
        errMsg = errorObj.error.errorMsg ? errorObj.error.errorMsg : errorObj.toString();
      }
      else {
        errMsg = errorObj.message ? errorObj.message : errorObj.toString();
      }
  
  
      console.error(errMsg);
      return throwError(errMsg || errorObj.message || errorObj);
    }
}

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../Services/auth.service';
import { SharedDataService } from '../Services/shared-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
public  showRegister:boolean=false;
public showRegisterMessage:boolean=false;
  user: any;

  constructor(public authService:AuthService,private sharedDataSvc:SharedDataService) { }

  ngOnInit() {
    this.authService.user$.subscribe(user => {
      this.user = user;
    });
    this.sharedDataSvc.showRegisterMessage$.subscribe(resp=>{
      this.showRegisterMessage=resp;
    })
  }
  newTab() {
   window.open('https://www.ksquareit.com/training');
  }
  onAuthTypeSwitch(authType: boolean) {
    this.showRegister=authType;
  }
}

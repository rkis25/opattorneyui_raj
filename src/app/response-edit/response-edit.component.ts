import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CaseHistory } from '../Models/caseHistory';
import { AuthService } from '../Services/auth.service';
import { CoreDataService } from '../Services/core-data.service';
import { SharedDataService } from '../Services/shared-data.service';

@Component({
  selector: 'app-response-edit',
  templateUrl: './response-edit.component.html',
  styleUrls: ['./response-edit.component.scss']
})
export class ResponseEditComponent implements OnInit {
  public isDisabled: boolean = false;
  public caseID: Number;
  // public questionHistory: { subject: string, question: string, response: string, date: Date }[] = [];
  public responseForm: FormGroup;
  public questionObj:CaseHistory=new CaseHistory();
  public responseHistory:CaseHistory[]=[];
  user: any;

  constructor(private fb: FormBuilder,  private route: ActivatedRoute, 
    private coreDataSvc:CoreDataService,
    private sharedDataSvc:SharedDataService,
    public authService: AuthService,) { }
  ngOnInit() {

    this.route.data
      .subscribe((data: { case: any }) => {
        if (data != undefined && data.case != undefined) {
          console.log(data);
          this.responseHistory = data.case;
          this.questionObj=data.case[0];
       this.caseID=this.questionObj.caseID;
        }
      });

    this.responseForm = this.fb.group({
      refID: [''],
      caseID: [this.questionObj.caseID],
      description: ['', Validators.required]


    });
    this.authService.user$.subscribe(user => {
      this.user = user;
    });
  }


  SaveResponse(){
    if(this.responseForm.valid){
      var reqObj={...this.responseForm.value}
      if(this.user.approle=="Paralegal"){
reqObj.isFinal=false;
reqObj.responseLevel="Paralegal";
      }
      else{
        reqObj.isFinal=true;
        reqObj.responseLevel="Attorney";
      }
      console.log(reqObj);
      this.coreDataSvc.addNewResponse(reqObj)
      .subscribe((resp: any) => {
        console.log(resp);
      //  if (resp.caseObj != undefined && resp.caseObj != null) {
          this.sharedDataSvc.showInfo("Response Send Successfully");
         //this.GetMyCaseHistory();
         this.responseHistory = resp.myCases;
          this.questionObj= resp.myCases[0];
         this.responseForm.reset();
         this.responseForm.controls['caseID'].setValue( this.caseID);
     

      }, err => {

        console.log(err);
        this.sharedDataSvc.showError("An error occurred while posting question, please contact support!");
      
      });
    }

  }
  // AddQuestion() {
  //   if (this.questionForm.valid) {
  //     var reqObj = { ...this.questionForm.value };
  //     reqObj.questionDate = new Date();
  //     console.log(reqObj);
  //   }

  // }

  

}

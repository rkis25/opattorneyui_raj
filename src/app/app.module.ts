import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/components/header/header.component';
import { FooterComponent } from './core/components/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AddQuestionComponent } from './add-question/add-question.component';
import { ProfileComponent } from './profile/profile.component';
import { ResponseListComponent } from './response-list/response-list.component';
import { ResponseEditComponent } from './response-edit/response-edit.component';
import { ContactusComponent } from './contactus/contactus.component';
import { AuthInterceptorService } from './Services/auth-interceptor.service';
import { LoaderInterceptor } from './Services/loader-interceptor.service';
import { LoaderComponent } from './loader/loader.component';
import { ToastrModule } from 'ngx-toastr';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { NgxUiLoaderModule, NgxUiLoaderHttpModule } from  'ngx-ui-loader';
import { ViewCaseComponent } from './view-case/view-case.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import {NgxPaginationModule} from 'ngx-pagination';
import { VerificationComponent } from './verification/verification.component';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    AddQuestionComponent,
    ProfileComponent,
    ResponseListComponent,
    ResponseEditComponent,
    ContactusComponent,
    LoaderComponent,
    ViewCaseComponent,
    VerificationComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FontAwesomeModule,
    FormsModule,ReactiveFormsModule,
    HttpClientModule,
    NgxMaskModule.forRoot(options),
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    NgxPaginationModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    NgxUiLoaderHttpModule.forRoot({showForeground:true}),
    NgxUiLoaderModule, // import NgxUiLoaderModule  { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    
  ],
  providers: [  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true },
    {provide: OWL_DATE_TIME_FORMATS, useValue: {
     
      datePickerInput: {year: 'numeric', month: 'numeric', day: 'numeric'},
      timePickerInput: {hour: 'numeric', minute: 'numeric'},
     
  }},
   ],
  bootstrap: [AppComponent]
})
export class AppModule { }

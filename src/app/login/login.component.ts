import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { faEnvelope ,faLock,faCheck} from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../Services/auth.service';
import { CoreDataService } from '../Services/core-data.service';
import { SharedDataService } from '../Services/shared-data.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public faEnvelope=faEnvelope;
  public faPassword=faLock;
  public faCheck=faCheck;
  @Output() switchAuth = new EventEmitter<boolean>();
  constructor( private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private coreDataService: CoreDataService,
    private sharedDataService: SharedDataService) { }

  ngOnInit() {
    this.mySignInForm = this.fb.group({

      email: ['', Validators.compose([
        Validators.required, Validators.email])],

      password: ['', Validators.compose([
        Validators.required])],
        signUpType: ['CANDIDATE']
     
    });
  }

  Switch() {
    this.switchAuth.emit(true);

  }


  mySignInForm: FormGroup;
  signIn() {
    if (this.mySignInForm.valid) {
      var credentials = this.mySignInForm.value;
      this.coreDataService.loginUser(credentials.email, credentials.password)
        .subscribe(
          data => {
            if (data.authToken != null) {
              var user=this.authService.getUserByToken(data.authToken)
                console.log(user);
              
              if(user && user.sub.role=='CANDIDATE')
              {
                this.authService._setSession(data, 'add-question');
            }
              else{
                this.authService._setSession(data, 'responses');

              }

            }
            else {
              this.sharedDataService.showError("User sign in failed!");
            }
          },
          error => {
            console.log(error);
            this.sharedDataService.showError(error);
          });
    }
  }

}



// degree: {
//   type: DataTypes.STRING
// },
// major: {
//   type: DataTypes.STRING
// },
// university: {
//   type: DataTypes.STRING
// },
// graduationDate: {
//   type: DataTypes.STRING
// },
// validationCode: {
//   type: DataTypes.STRING
// },

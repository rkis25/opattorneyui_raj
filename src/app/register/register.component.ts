import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { faUser,faPhone, faEnvelope, faLock, faCheck,faGlobe } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../Services/auth.service';
import { CoreDataService } from '../Services/core-data.service';
import { SharedDataService } from '../Services/shared-data.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @Output() switchAuth = new EventEmitter<boolean>();
  public faUser=faUser;
  public faPhone=faPhone;
  mySignUpForm: FormGroup;
  public faEnvelope=faEnvelope;
  public immigrationStatus="OPT 12-month";
  public faPassword=faLock;
  public faCheck=faCheck;
  public faGlobe=faGlobe;
  constructor( private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private coreDataService: CoreDataService,

    public sharedDataService: SharedDataService) { }

  ngOnInit() {
    this.mySignUpForm = this.fb.group({


                                
      // username: [this.user.username, Validators.compose([Validators.required])],
      firstName: ['',Validators.compose([Validators.required])],
      middleName: [''],
      lastName: ['',Validators.compose([Validators.required])],
       immigrationStatus: [this.immigrationStatus,Validators.compose([Validators.required])],
       immigrationStatusText: [""],
      email: ['', Validators.compose([
        Validators.required, Validators.email])],
      password: ['',Validators.compose([Validators.required])],
     
      mobilePhone: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(10)])
      ],
      signUpType: ['CANDIDATE'],
      disclaimerStatus: [null, Validators.compose([
        Validators.required])]
    });
  }
  Switch() {
    this.switchAuth.emit(false);

  }
  signUp() {
    if (!this.mySignUpForm.controls["disclaimerStatus"].value) {
      this.sharedDataService.showWarning("You need to accept terms & conditions to create an account!");
      return;
    }
    // if (!this.mySignUpForm.controls["disclaimerStatus"].value) {
    //   this.sharedDataService.showInfo("You need to accept terms of Use & Privacy Policy to create an account!");
    //   return;
    // }

    if (this.mySignUpForm.valid) {

      this.register(this.mySignUpForm.value);
    }
    else {
      this.sharedDataService.showWarning("Invalid Information, Please fill correct info!");
    }


  }

    
  register(credentials) {

    this.coreDataService.registerUser(credentials)
      .subscribe(
        data => {
          if (data.authToken != null) {
            this.sharedDataService.showSuccess("User Registered Successfully!");
          //  console.log(credentials);

   
         this.sharedDataService.showRegisterMessage$.next(true);
         //send confirmation email here instead of sending session
          // this.authService._setSession(data, 'home');

          }
          else {
            this.sharedDataService.showError("User sign up failed!");
          }
        },
        error => {
          console.log(error);
          this.sharedDataService.showError(error);
        });
  }


}

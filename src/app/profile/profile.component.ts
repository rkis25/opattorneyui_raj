import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../Services/auth.service';
import { CoreDataService } from '../Services/core-data.service';
import { SharedDataService } from '../Services/shared-data.service';
import { MyProfile } from '../Models/myProfile';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
public faGlobe=faGlobe;
  profileForm: FormGroup;
  public myProfile: MyProfile=new MyProfile();
  @Output() switchAuth = new EventEmitter<boolean>();
  user: any;
  constructor(

    public sharedDataService: SharedDataService,
    private fb: FormBuilder,
    public authService: AuthService,
    public coreDataService: CoreDataService,
    public sharedDataSvc: SharedDataService
  ) { }

  ngOnInit() {
    this.authService.user$.subscribe(user => {
      this.user = user;
    });
    this.profileForm = this.fb.group({

      // visaStatus: [this.myProfile.visaStatus, Validators.compose([
      //   Validators.required])],
      degree: [this.myProfile.degree, Validators.compose([
        Validators.required])],
      major: [this.myProfile.major, Validators.compose([
        Validators.required])],
      university: [this.myProfile.university, Validators.compose([
        Validators.required])],
      graduationDate: [this.myProfile.graduationDate, Validators.compose([
        Validators.required])],
     
      immigrationStatus: [this.myProfile.immigrationStatus, Validators.compose([
        Validators.required])],
      immigrationStatusText: [this.myProfile.immigrationStatusText]



    });
    this.coreDataService.getMyProfile().subscribe((resp: any) => {
      console.log(resp);
      if (resp.myProfile) {
        this.myProfile = resp.myProfile[0];
   

   this.profileForm.patchValue({
    candidateID:this.myProfile.candidateID,

    degree: this.myProfile.degree,

    email:this.myProfile.email,
  
 
    firstName: this.myProfile.firstName,

    graduationDate:this.myProfile.graduationDate,
    immigrationStatus: this.myProfile.immigrationStatus,
    immigrationStatusText:this.myProfile.immigrationStatusText,
   
    lastName: this.myProfile.lastName,
    major: this.myProfile.major,
    mobilePhone: this.myProfile.mobilePhone,
    university: this.myProfile.university
  });
  if(this.myProfile!=undefined && this.myProfile!=null && this.myProfile.immigrationStatus!="" && this.myProfile.immigrationStatus!=null && this.myProfile.immigrationStatus!=undefined  ){
    this.switchAuth.emit(true);
  }

      }
      

    }, err => console.log);

   
  }
  SaveProfile() {
    if (this.profileForm.valid) {
      var credentials = this.profileForm.value;
      this.coreDataService.saveProfile(credentials)
        .subscribe(
          data => {
            if (data.profile != null) {

              this.sharedDataService.showSuccess("Profile Completed!");
              this.switchAuth.emit(true);
            }
          },
          error => {
            console.log(error);
            this.sharedDataService.showError(error);
            this.switchAuth.emit(false);
          });
    }
  }



}

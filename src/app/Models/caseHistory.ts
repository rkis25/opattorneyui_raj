export class CaseHistory{
  askedBy:string;
    assignedAttorney: string;
    attorneyResponseDate: Date;
    caseHeader: string;
    caseID: Number;
    caseStatus: string;
    description: string;
    questionDate: Date;
    response: string;   
    paralegalResponseDate:Date
  }

//   askedBy: "Abhishek Dixit"
// assignedAttorney: "John Doe ESQ"
// attorneyResponseDate: null
// caseHeader: "What happens next when H1 expires?"
// caseID: 4
// caseStatus: "PENDING"
// caseStatusDesc: "Pending, Awaiting Attorney Response"
// description: "my points are about to expire, please assist"
// questionDate: "2019-11-01T12:30:56.000Z"
// paralegalResponseDate: null

// lastResponseBy: "John Doe ESQ"
// response: null
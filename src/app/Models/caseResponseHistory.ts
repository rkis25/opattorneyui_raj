export class CaseResponseHistory {

  askedBy: String;
  attorneyResponseDate: Date;
  caseHeader: String;
  caseID: Number;
  caseStatusDesc: String;
  description: String;
  lastResponseBy: String;
  paralegalResponseDate: Date;
  questionDate: Date;
  response: String
}
  
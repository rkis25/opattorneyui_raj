export class MyProfile{
    candidateID: string;
      firstName:string;
      lastName: string;
      visaStatus: string;
      immigrationStatus:string;
      immigrationStatusText:string;
      mobilePhone : string;
      password: string;
      email: string;
    
      passwordResetToken: string;
      disclaimerDate: string;
      disclaimerVersion: string;
      notificationPreference: string;
      degree:string;
      major:string;
      university: string;
      graduationDate: Date;
      validationCode:string;
  
  
  
  
      facebookID: string;
      facebookToken: string;
      facebookName:string;
      gmailID: string;
      gmailToken: string;
      gmailName: string;
      isProfileComplete: boolean;
    
      
      
}